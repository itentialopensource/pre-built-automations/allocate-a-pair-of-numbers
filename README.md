<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

<!-- Update the below line with your artifact name -->
# Allocate a Pair of Numbers

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Features](#features)
* [Future Enhancements](#future-enhancements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview
This Pre-Built consists of a JST document that finds the first available consecutive pair of numbers from a given starting integer, a given ending integer, and a given array of already allocated integers (`allocated`).

For instance, a range is defined by start and end values: ex: 0 through 255. These values are assumed for the following examples.

Example 1: `allocated: [12,200,199,0,2,4,100,101,150]`. The output from this JST is an object containing the next available consecutive pair of numbers availible in the range. `{ "assigned": [5,6] }`.

Example 2: `allocated: [187,19,0,1,2,18,10,140]` The output for this JST will be `{ "assigned": [3,4] }`.

In case the range is all allocated, the output object will be:  `{ "assigned": false }`

Example use cases when this JST can be used: an array of ports, an array of IP addresses (can only handle 1 quadrant: 0-255), etc.
<!-- Write a few sentences about the artifact and explain the use case(s) -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: depends on the size of the array. usually within second(s)

## Installation Prerequisites

Users must satisfy the following pre-requisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1.x`


## Features

The main benefits and features of the Pre-Built are outlined below.
 * Finds the first available consecutive pair of numbers within an array. 
<!-- Unordered list highlighting the most exciting features of the artifact -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## Future Enhancements
 * Increasing an algorithm's efficiency by converting logic onto a recursive call.
<!-- OPTIONAL - Mention if the artifact will be enhanced with additional features on the road map -->
<!-- Ex.: This artifact would support Cisco XR and F5 devices -->

## How to Install

To install the artifact:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built.
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired artifact and click the install button.

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this artifact can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

In order to run this JST, add the appropriate JST task onto the workflow, then search for "allocateNewPair" and
assign values to all incoming variables:

    startRange: INT
    endRange: INT
    allocated: ARRAY of INT
<!-- Explain the main entrypoint(s) for this artifact: Automation Catalog item, Workflow, Postman, etc. -->

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built Transformation.
